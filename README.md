# Karaoke Mugen documentation

This is the repository for Karaoke Mugen's documentation.

It generates [this part of the site](https://docs.karaokes.moe)

## Install

Make sure you pulled in git submodules to get the theme to work.

You'll need [Hugo](https://gohugo.io) for this. Please download the **extended** version.

## Testing

Once installed, you can use `hugo serve` and lead your browser to http://localhost:1313
