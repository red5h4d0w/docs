+++
title = "Streamer Guide"
weight = 9
+++

You have a Twitch channel and you like doing karaoke streams? Karaoke Mugen might be made for you (we hope so!)

The app has several options and features to help you with your karaoke stream. Here's what you need to know to get started.

## Download Karaoke Mugen

For that, [take a look at the install section in this doc](../install).

On its first launch, Karaoke Mugen will download its database, but without media files.

For more information on how to use Karaoke Mugen, [see the "Getting Started" section](../getting-started)

## About medias

All the media files in Karaoke Mugen weight several hundreds of gigabytes, but you don't have to download everything.

We recommend you to pre-download all the songs via the system panel if you have enough storage available, though, to avoid using your internet connection or Karaoke Mugen's servers during your stream.

If you cannot or do not want to download all medias in advance, don't panic: Karaoke Mugen will download them during your karaoke session like this:

- If you play a song directly from the library, it won't be downloaded but will be streamed instead from Karaoke Mugen's server.
- If you add a song to the current playlist (the one used by the video player), it'll be downloaded in the background.

If you're short on disk space, Karaoke Mugen will stream from the server. You can delete downloaded medias from the system panel to make some room.

## About the data folder

When we speak of **data folder**, it's generally the one where Karaoke Mugen stores its user files.

On Windows, it's this `C:\Users\yourUser\KaraokeMugen` by default.

On macOS or Linux, it's the `~/KaraokeMugen` folder (it's at the root of your user home folder)

## Prepare your layout

### Video player

The video player will display songs and lyrics. You can resize the windows as you wish and add it to your layout with OBS/Streamlabs.

You can put it in full screen in options, or click on it and hit the F key on your keyboard. Press F again to turn it back to windowed mode.

### The current playlist window

A current playlist window can be enabled from the app's menu bar, on the **Window** menu. Capture this window to display the currrently playing song, a progress bar as well as the songs to follow. You can resize the window as you wish to fit in your layout.

### Info text files

Karaoke Mugen will write informations on the current karaoke session in text files, that you can watch and integrate in OBS to display these wherever you want on your layout.

These files are in the `streamFiles` folder in the data folder. Here's what's inside each one:

- `song_name.txt` : Currently playing song
- `requester.txt` : Whoever requested the song.
- `km_url.txt` : URL to access your karaoke interface (`http://abcd.kara.moe`)
- `frontend_state.txt` : Interface status (Opened, Closed, Restricted)
- `current_kara_count.txt` : Number of songs in the current playlist.
- `public_kara_count.txt` : Number of songs in the public (suggestions) playlist
- `time_remaining_in_current_playlist.txt` : Remaining time in the current playlist with a `1h20m` format, for example

## Customize Karaoke Mugen

### Background

The default player's background can be modified via the system pannel, in the "Backgrounds" page. A random background will be loaded during each pause if there are different backgrounds available.

### Pause music

By default there's no background music bundled with Karaoke Mugen.

You can however add one yourself via the "Backgrounds" page in the system panel by drag and dropping music files (mp3, m4a, ogg, flac, wav...) inside. They'll be read during pauses between songs.

If a music file has the same name as an image file (without its extension), it'll be played along with the image file if it's randomly selected. If not a random music file will be chosen.

## Configure Karaoke Mugen

Let's get to it.

It all depends on what you'd like to do with your karaoke session.

### You want to keep control on your karaoke

No worries, you can prepare a playlist in advance and play it, or ask your Twitch viewers to type song names that you'll select by yourself, if needs be, just like in old times.

### Allow your viewers to participate

#### Giving access to your karaoke interface

Before anything else, make sure that **remote access** is enabled (it should be by default) and that it managed to connect to Karaoke Mugen Server. If not, your viewers won't be able to log in onto your Karaoke Mugen interface through the Internet.

If you see an access URL in the video player like `https://xxxx.kara.moe` then it's all good. If not, make sure you have the latest Karaoke Mugen version and that it's not being blocked by your firewall or antivirus. If the problem is still there, [contact us](https://mugen.karaokes.moe/contact.html).

The displayed URL will be the one you'll give to your viewers, either via your layout, or maybe a bot that you'll have previously configured.

The first four letters at the beginning of the URL will changei f you don't use Karaoke Mugen during more than 15 days. If you want a custom, permanent URL, [it's a Patreon reward](https://patreon.com/karaokemugen)

#### Options

##### Stream

In the Karaoke Mugen option menu (Operator Interface > K Menu in the upper right > Options) go to the **Karaoke** tab.

- Enable **Stream Mode**
- Set the **Pause Time** to how long you wish (30 seconds, for example). If you need to make the pause longer, click on the **Stop** button to pause the pause screen (yes.)
- Enable **Twitch Chat**. You'll need an *OAuth token*. There's a link in the option panel to allow you to get it. It'll allow Karaoke Mugen to erad and write on your Twitch Chat, like a bot.

If Twitch Chat is enabled, Karaoke Mugen will reply to the following commands:

- `!vote` : see below
- `!song` : displays the curerntly playing song.

##### Live Comments

If you have given your Twitch OAuth Token like described above, you can enable the "Live Comments" in the quick settings menu in the upper left corner of the Operator Interface.

When people will speak on your twitch chat, their messages will appear on your video player by scrolling from right to left, just like on the Nico Nico Douga site.

##### Intermissions

Depending on your preferences, you might want to disable jingles between songs, encore, outros, sponsors and intros intermissions. But we believe you should keep them enabled. They're pretty funny and people died to make them. Almost.

##### Limits

To limit abuse you can define how many songs your viewers can suggest at once. A song is "freed" and quota given back whenever the song is played or added to the current playlist (whichever comes first)

Quota can be:

- **Disabled**: Viewers can suggest as many songs as they want, but it can take a long time for you or your karaoke operator to choose among them.
- **By number of songs**: For example, 2 or 3 songs at most.
- **By time**: to avoid people requesting long songs, you can limit the time in seconds they're allowed to add.

#### Playlists

Generally, you'll want to create a new "Suggestions" playlist and give it the "Public" property. That's the list your viewers will add their suggestions to.

By displaying on one side the public playlist and on the other the current playlist, you can refuse or accept songs.

- Refuse them: they'll stay in the public playlist so they won't be suggested again, and will free the quota of the viewer who requested it.
- Accept them: they'll be sent to the current playlist so the player can play them. They'll be freed as well.

Either you accept/refuse from the list itself, or you can review songs easily by clicking on one of the songs. Once on its details page, you'll have buttons to accept/refuse the song, and it'll then switch to another one to review.

#### Blacklist

The blacklist allows you to keep some songs from being viewable and selectable, if you absolutely do not want to see some singers, some series, or karaokes you shouldn't show on stream.

Some examples:

- If you don't like mecha anime, you can hide all songs from that genre by banning the "Mecha" genre.
- Songs with the "Adult Only", "Spoiler" or "Epilepsy" tags.

These criterias are modifiables from the "Criterias" view in the playlist with the [blacklist](../playlists) property. You can have several blacklists depending on your mood.

#### Song polls

Another popular way to allow your viewers to participate is to enable the Poll Mode. How does it work?

- Fill the public (suggestions) playlist with the songs you like to sing.
- Put one or two songs from that list in the current playlist.
- Enabled th Poll Mode in Karaoke Mugen Options (in the Karaoke tab)
- Set a poll time **lower** than the pause time you've set under Stream Mode, so that people will have enough time to see the poll results.

When you'll start singing, during each pause, viewers will be able to vote among N choices from the suggestion list. They'll be displayed on the screen as well as in your Twitch Chat

There are several ways for your viewers to vote:

- Via Twitch Chat with the `!vote n` command, where `n` is the song number displayed on screen.
- Via the web interface if you've allowed them access.

## During your stream

There it goes, it's your moment!

Launch Karaoke Mugen, change your layout, and you're ready!

### Name your session

On the welcome screen, in the upper right corner, you can change the current karaoke session and rename it. This allows you to better identify your karaoke sessions between each other and find them again later (to make stats for example!)

You can also make some sessions private (statistiques won't be uploaded then.)

### Open the interface to your viewers

Don't forget to select the openness level of your inteface to your viewers:

- **Closed** : A page will tell them to wait.
- **Restricted** : Viewers will only be able to see the currently playing song and the current playlist.
- **Opened** : Viewers can suggest songs.

Switch to **Restricted** mode towards the end of your karaoke, whan you're sure you don't want any more suggestions.

### Name one or several co-operators

Once your faithful moderators registered on your Karaoke Mugen app, you can name them "operators" as well so they can help you manage your karaoke, for example by modifying the playlist in realtime, accept or refuse songs, etc.

To do that, once their account is created, you can go in the System Panel and in the "User List" page. Find them, edit their account and change their rank to Operator!

### Let's go!

You're now a karaoke star!
