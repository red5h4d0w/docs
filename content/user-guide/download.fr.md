+++
title = "Télécharger des karaokés"
weight = 5
+++

## Base de karaokés

### Télécharger des karaokés

Au premier démarrage Karaoke Mugen va récupérer l'intégralité des chansons du dépôt par défaut. Cependant les médias ne seront pas téléchargés pour économiser votre espace disque.

Les chansons dont vous avez besoin seront téléchargés automatiquement lorsque vous les ajoutez à une liste de lecture courante.

Vous pouvez ensuite télécharger les karaokés de votre choix depuis le panneau système, ou depuis le bouton "Téléchargements" de l'écran d'accueil.

![Download](/images/user/download.png)

Vous pouvez également télécharger l'intégralité des karaokés mis à votre disposition par la communauté, pour cela il vous suffit de cliquer sur "Tout télécharger".

![Download_All](/images/user/download_all.png)

{{% notice warning "Attention" %}}
Les vidéos prennent au bas mot plusieurs centaines de giga-octets, assurez-vous d'avoir suffisamment d'espace disque disponible.
{{% /notice %}}

{{% notice tip "Quitter l'app et reprise des téléchargements" %}}
Vous pouvez quitter Karaoke Mugen à tout moment et reprendre vos téléchargements plus tard.
{{% /notice %}}

Ceci dit, vous n'avez peut-être pas envie de récupérer toute la base en entier, mais seulement les karaokés qui vous intéressent. Pour cela, vous avez plusieurs façons de sélectionner ou trier vos choix. Il y a un champ de recherche, bien sûr, mais aussi un filtre de tri sur la droite, qui vous permet de n'afficher que les karaokés ayant une valeur spécifique. Par exemple, si je veux récupérer uniquement les karaokés en breton, il faut aller dans le champ "Filtre des tags" cliquer sur "Langues", puis sur "bre" pour faire apparaître tous les karaokés possibles en breton :

![Download_Bre](/images/user/download_fre.png)

Une simple recherche sur le terme "breton" fonctionne également.

Une fois que la liste affichée est celle voulue, vous pouvez soit télécharger les chansons de votre choix avec le bouton en bout de ligne, soit télécharger tous les karaokés affichés avec le bouton en haut de la colonne :

![Download](/images/user/download_button.png)

### Créer votre propre base

{{% notice warning "Attention" %}}
Réservé aux utilisateurs avancés.
{{% /notice %}}

Pour cela, direction la page sur comment [gérer votre base de karaoké](../../contrib-guide/manage/), notamment si vous avez déjà des vidéos + fichiers .ass de karaoké à disposition.

### Mettre à jour la base des médias

* En utilisant le panneau système, comme indiqué ci-dessus, cliquez simplement sur "Synchroniser" pour récupérer les dernières nouveautés de vos dépôts configurés.
* Vous pouvez également lancer **Karoke Mugen** en ligne de commande avec l'option `--updateMediasAll`.

### Dépôts

Les dépôts permettent de séparer les karaokés de la base principale communément appelée "kara.moe". La base kara.moe est ajoutée par défaut à votre logiciel lors de l'installation, mais si d'autres communautés créent des karaokés qu'ils ne veulent pas voir figurer dans la base "kara.moe" pour une raison ou une autre (des dépôts privés en somme) ils pourront être ajoutés dans votre application pour que vous puissiez vous fournir chez eux !

Vous pouvez les configurer en cliquant sur "Dépôts de karaokés" sur la page d'accueil du panneau système. Vous pouvez en ajouter de nouveaux via le bouton d'ajout, nous vous invitons à vous rapprocher des mainteneurs du dépôt qui vous intéresse si vous avez du mal à le configurer dans Karaoke Mugen.

![Dépôts](/images/user/repos.png)
