+++
title = "Playlists and Player"
weight = 3
+++

Karaoke Mugen works around playlists, the [operator interface](../getting-started/#operator-interface) shows you two lists. You are able to make actions between these two playlists (add song from left to right, or the other way, by copying it or moving it). We will explain all that to you.

## Lists concepts

The karaokes are shown line-by-line, with in order: the karaoke's language, their series (or singers if there isn't), their type (Opening, Ending, MV, etc.) and their name. The lines with an <i class="fas fa-history"></i> icon highlights the karaokes that was played on screen less than an hour ago (this delay is tweakable), useful to avoid repeats. The blue line shows the karaoke positioned for playing (the one that will be played if the playlist is [current](#current-playlist)).

{{% notice info "About language preferences" %}}
You can define your linguistic preferences for the series names (Romaji, Native japanese, English, etc...) in the profile settings, available in the K menu in the up-right corner.
{{% /notice %}}

## Library <i class="fas fa-book"></i>

The library shows all the karaokes that the software is aware of. These are the ones from all configured repositories or ones you [created](../../contrib-guide/). They are alphabetically sorted by the series or the singers if there isn't any series. You can change sort mode by clicking the filter <i class="fas fa-filter"></i> near the list selector.

## Playlists <i class="fas fa-list-ol"></i>

You can create as many playlists as you want. These lists can be useful to prepare for karaoke sessions and parties, or listen to various songs depending on your mood. A playlist can have as many songs as you want, but a song can only be present once in a list.

You will have one **current** playlist, one **public** playlist, one **black**list and one **white**list that will fulfill special roles. There is a little schema to sum it up.

![](/images/user/playlists_schema.png)

### Public playlist <i class="fas fa-globe"></i>

This is the playlist that will receive all the songs that users will add via [the public interface](../getting-started/#public-interface). Only one list can be the public playlist. To designate one as public playlist, you need to click on Edit playlist in the wrench <i class="fas fa-wrench"></i> menu and to check Public checkbox (this list will replace the old public playlist).

### Current playlist <i class="fas fa-play-circle"></i>

The current playlist is the playlist used by the [video player](#video-player) and is controlled by the play/pause, next, previous buttons in the [operator interface](../getting-started/#operator-interface) header. The line displayed in blue is therefore the karaoke now playing. There can be only one current playlist. To designate one as current playlist, you need to click on Edit playlist in the wrench <i class="fas fa-wrench"></i> menu and to check Current checkbox (this list will replace the old current playlist).

### Current AND public playlist <i class="fas fa-play-circle"></i> <i class="fas fa-globe"></i>

Having a list that fulfills both roles is not forbidden: as currennt and public list, the playlist will be playing normally while receiving the public suggestions in realtime. This is useful if you trust yoru guests and don't want to filter what they want to sing.

However, if you have two separate lists, one public and one current, you can filter suggestions from your guests before sending them to the video player!

### Blacklist

Songs in a blacklist won't appear in search results for your users anymore. Operators can still see them. However, if you add blacklisted songs to a playlist, users will still **be able to see them**.

You'll want to make a "smart" blacklist using criterias, but you might also want a blacklist where you manually put each song you want to forbid.

If you want to add exceptions, you can use the [whitelist](#whitelist).

### Whitelist <i class="fas fa-check-circle"></i>

In this list, you can add exceptions to criterias you defined earlier. For exemple, if you ban all *mecha* karaokes, you can decide to keep King Gainer, because it's cool, by adding it to the whitelist.

## Smart playlists

Karaoke Mugen has a smart playlist system which works with criterias, these smart playlists can take on the roles explained above.

Songs fullfilling criterias set will appear in the list automatically. if new songs are added to the library, they'll also be added automatically.

A smart playlist can't be converted to a simple one. Same goes for the other way.

### Smart playlist criterias

You can switch from the list view to the criterias view on a smart playlist.

Criterias examples:

- **Title containing** : For example "Kiseki"
- **Longer than/shorter than** : All songs longer or shorter than X seconds. It's pretty handy to ban AMVs or longer songs.
- **Tags** : Particular tag
- **Singer**
- **Type** : Songtype (Opening, Ending, Clip, AMV, etc.).
- **Creator** : Who created the song.
- **Karaoke author** : Who created the karaoke (who worked on the karaoke file)
- **Language**
- **Misc** : Miscellaneous tags
- **Songwriter**
- etc.

Let's take a blacklist for example: it can be a smart one and have criterias. You can ban songs longer than X minutes, those with a spoiler or for adults tag, etc.

Back to smart playlists, you can also tune some of its settings:

- Limit the list to X songs or a duration of X while sorting it. It can be handy if you want to make a "Top 100" smart playlist.
- Choose between union or intersection (OR/AND) :
  - If you select "OR", every criteria will add its own songs to the smart playlist. For example "Tag: For Adults" Or "Series: Gundam" will add **all songs for adults + songs from Gundam**.
  - If you select "AND", a song will only be added if it **satisfies all criterias**. For example "Series: Pokemon" AND "Duration: less than 2 minutes" will select all Pokemon songs which last less than 2 minutes. With "AND" and some exclusive criterias it's possible that absolutely no song will get into the smart playlist (For example if you select "Duration: longer than 3 minutes" and "Year: 1982" since no song satisfies both criterias at the same time)
  
For a smart blacklist (which is used to ban some songs) you'll want to use the "OR" setting.

For a general smart playlist, "AND" will be more useful.

## Favorites <i class="fas fa-star"></i>

Each user has its own favorites list in which they can find quickly the karaokes they like. If you have an online account, this list is viewable from the [database website](https://kara.moe/), by logging in to your account.

To add a song to favorites, you can click the wrench <i class="fas fa-wrench"></i> button in a karaoke line then click the "Add to favorites" button. In public interface, you can click on a karaoke and click also on "Add to favorites" button.

Users can use their favorites list to quickly suggest songs to the operator.

### AutoMix lists <i class="fas fa-bolt"></i>

An AutoMix is a playlist generated automatically from selected users favorites, for a selected duration. To create an AutoMix, you can click on the gear button <i class="fas fa-cog"></i> near the list selection.

The list generated by AutoMix can be used as [current](#current-playlist) or [public](#public-playlist).

### Likes

When checking out the public playlist, users can *like* a particular song to tell the operator they'd like to see it play soon. This allows the operator to get a better idea of which songs are more popular.

As every user has a limited number of songs they can add during a karaoke session, if the song it requested (the first Naruto opening for example) gets *liked* enough, it won't count anymore in the user's quota. The user who requested the song can then request another song freely without having to wait for the opening to be played on screen. This free song mechanic can be entirely configured.

Operators can free songs manually via the wrench menu of each karaoke.

### Video player

The video player used is mpv, which you can find on its website, [mpv.io](https://mpv.io).

{{% notice tip "Some useful keyboard shortcuts for mpv" %}}
* `ESC` : Leave fullscreen
* `Q` : Exit player (this does not exit Karaok Mugen!)
* `Left/Right arrow` : Forward/Rewind in a video
* `Up/Down arrow` : Forward/Rewind faster in a video
* `F` : Switch between fullscreen and windowed mode.
* `Mouse wheel up/down` : Change volume (Mouse cursor must be over the window)

These keys only work if you have "focus" on the video player. Click inside the window to get focus.

{{% /notice %}}

You can also move the window around on your screen. By default it is always on top, but this can be changed.
