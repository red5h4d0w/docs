You may have noticed that some names have diacritics, for example the artist name **_Mâya Uchida_**.

There are standard writing rules for names here.

- A name with a long "o" (ou) is written "ô".
	- Sometimes some names are written "oh", as "Yuusha-Oh Tanjou". This is the hepburn english transliteration, which is "ou" in modified hepburn. *"Oh"* stands for *"ô"* here too.
- A name with a double "o" (oo) **stays** as "oo".
- A name with a long "u" (uu) is written "û".
- Un nom avec a long "a" (aa) is written "â".
- A name with a long "e" (ee) is written "ê".
- A name with a long "i" (ii) **stays** "ii".
- A name with an apostrophe (Shin'ichi for example), keeps it.
