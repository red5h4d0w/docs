+++
title = "FAQ"
weight = 7
+++

## Erreur type "Media file not found"

Vérifiez bien que votre vidéo existe et qu'elle porte le bon nom. Méfiez-vous des minuscules/majuscules surtout sous Windows. Vérifiez ensuite qu'il n'y a pas de double espace. Si vous êtes absolument certain que c'est OK, mais que ça ne marche toujours pas, renommez le fichier sous un autre nom puis renommez-le encore avec son vrai nom : des caractères parasites peuvent parfois se faufiler et apparaître comme des espaces, mais ne sont pas considérés comme de vrais espaces !
