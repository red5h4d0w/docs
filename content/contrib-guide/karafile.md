+++
title = "Karaoke Files"
weight = 4
+++

This is important: it tells Karaoke Mugen where are the files to use and some other informations to generate its database.

Examples of metadata about your karaoke: the singer’s name, the songwriter’s one, the studio that made the video, the creation date or the language used to sing.

Here, we will see how to create or modify the file via a graphical user interface.

## User interface access

- The karaoke management user interface is in Karaoke Mugen. Run the software and on the home screen, click on the button SYSTEM PANEL (you can also access it via the go to > system panel menu)

![ass](/images/creation/System-EN.jpg)

- Once in the system tab, go to the **Karaokes** emnu then "New" to access the .kara.json creation form.

![ass](/images/creation/SystemTab-EN.jpg)

## Fill the karaoke information

You now see the form to create songs.

![ass](/images/creation/Karacreatepage-EN.jpg)

The series, as all other elements, are tags. Tags have their own [data files](../tag), that contain region variations and aliases.

You will have either checkboxes, all you have to do then is check the correct ones, or a field with autocomplete that will let you create tags if it doesn't exist.

If you need to create tags, you can, after the karaoke creation, edit tags to add additional information.

### Media file

This is your video source (or audio if it's audio only). Click on this button and grab your file to add it. You can also drag & drop it directly.

### Subtitle file

This is your *time*, your *.ass*. Click on the button and go grab it or drag & drop it.

### Parent Karaokes

By selecting a parent song, KM will copy its information to the new one, allowing you to edit what changes between the parent and child. It also defines the song as a "child" of the parent. This means users will see that the parent has several chidren versions. For example it can be all versions of the Sakura Taisen opening, or a *music video* version of an *opening*.

By selecting a parent, users will see all their versions at once.

A song can have more than one parent.

### Song titles

This is the song title for your karaoke. It can exist in several languages and have aliases. Aliases allow you to specify words not found in any other title to find your karaoke more easily in the search engine.

{{% notice tip "About aliases" %}}
It's utterly **useless** to add words in aliases which are **already** in the different titles in other languages, this will only slow down the search engine.
{{% /notice %}}

A song should only have one title, but sometimes it can have several depending on the language, like in english and japanese for example.

{{% notice note "About songs in japanese" %}}
Check out [our typography section below](#typography) on how to write a japanese title correctly

You must add the title in "Japanese" (in kana) and in "Japanese - Romaji" in romaji.

Default title language should be "Japanese - Romaji"
{{% /notice %}}

### Versions

The Versions tags let you specify a song alteration, useful for covers, full versions, etc. When you are creating your karaoke, versions will be available with checkboxes (you can check many of them).

If you have to specify a version that isn't in the list (for example a specific episode ending, or the ending sung by another one), you can specify it by adding it to the title, following this model `<title> ~ <version> Vers.`.

For example ["irony ~ Ep. 12 Vers."](https://kara.moe/base/kara/irony-ep-12-vers/b9e80280-0d04-415a-bca3-2fd2681043e2), notice that the alternative version tag is present.

Do not forget to also add the correct version tags (when applicable) even if you use that manual version notation. Most of them will need the *Alternative* version tag, for example.

{{% expand "About the Full, Short, Concert tags, live, and everything else" "false" %}}

Do not use *Full* tag on live events. Besides that special cse, if you add a full version of a song, check the *Full* version tag only if this song already exists in the database as another version (like a short one, typical if you add a MV of an OP song).

Example :  

- `ENG - Kiznaiver - OP - LAY YOUR HANDS ON ME` et  
- `ENG - Kiznaiver - MV - LAY YOUR HANDS ON ME [Version Complète]`

Never add the *Short* version tag on a song unless there's already a Full version existing in the database.

{{% /expand %}}

### Series

If your song does not have a series, TV or Movie (it's just a song), you can skip this part.

You can add here the series the song is from (or several series if you add an AMV).

![ass](/images/creation/KaracreatepageSerie-FR.png)

### Song type

{{% expand "About non official videos" "false" %}}
**A non official / amateur video is an AMV. An official video is an MV.**
{{% /expand %}}

{{% expand "About audio-only songs" "false" %}}

If you add an MP3, pick the type that fits the closest the song. If for example it's a song that you hear in the series (even as of an extract), put IN (Insert song), OP(Opening), ED (Ending) depending on the situation. If it's a full version, pick one of those [without forgetting to add ~ Full Vers.](#specific-rules).If the song's not present in the series, you can either name it `character song` or `image song`, or even `other`. If it is from a single or an album, tag it as AUDIO.

{{% /expand %}}

If the song isn't present in the series, you can use **character song** or **image song**, or even **other**. If it's a song from a single or album, use simply **Audio only**.

Use **other** only as a last resort.

{{% expand "Differences between Character Song and Image Song" "false" %}}

- An Image Song is a song used in a promotionnal way and that is **unused** in the anime (for example the album "Negai ga Kanau Basho ~Vocal&Harmony version~" from CLANNAD is an image album and so contains image songs).

- A Character Song is a song used in a promotionnal manner, unused in the anime, and that's meant to give indepth information about the character it involves. It  is **not always sung** by the voice actor. You can find Character Songs either in music albums dedicated only to them, or inside OSTs.

- Do not confuse these with a Character Version, which is a cover of a song sung by another chracter than the original one, (example, the Hare Hare Yukai sung by Kyon). In this case you must not forget to add  `~ *character's name* Vers.` in the song title.
{{% /expand %}}

### Song number

Song numbers are usually only for japanese anime songs.

{{% expand "About song numbers" "false" %}}

We number **only openings and endings**, we do not number other song types.

- If the series has only one opening / ending, this  must be left **empty** (and not filled with "0").
- On anidb, if the first episode has an ending which is the same as the opening of the next episodes, DO ignore it, that is NOT an ending, it is first and foremost the opening of the series.

{{% /expand %}}

### Language(s)

The langage in which your karaoke is sung. Like with everything else there is a list.

- If the langage is unknown / made up, add **"Unknown language"**.
- If your karaoke has no singing (and that your .ass is empty), add **"No linguistic content"**.
- You can add up to two langages to a karaoke. If the song has more than two languages, please use **"Multi-language"**.
- If your karaoke has as much or *almost* as many lines in "language 1" as in "language 2", add both languages. However, if a langage takes over the other one, add only the first one.
- If you have made an *instrumental/off-vocals version* (without any actual singing), choose the langage the lyrics use.

### Broadcast year

The year **when your video was broadcasted first** (and not the year the series has started, do not put "1999" on the season 18 of One Piece).

### Singer(s)

The performer(s) of your karaoke. Like for series, there is already a list, verify that your singer(s) are not present already so not to create a dupe.

### Songwriter(s)

The author(s), composer(s), arranger(s) **and** lyricist(s) of your karaoke. As with series, a list is already here, verify that the composer(s)/arranger(s)/lyricists(s) are not already in there to avoid creating a dupe.

### Creators(s)

The entity which created the song (often animation/game studios but it can be only music labels if it's for a music video).

{{% expand "Depending on your song type..." "false" %}}

- If you wrote a karaoke for an AMV, put the studio **and** the AMV maker.
- For video games, put the entity which **developped** the game, not the one which sells/publishes it.
- If it's a Music Video, put the band/group's name.
{{% /expand %}}

### Karaoke authors

It's you! Write down your name or username to add yourself to the "Karaoke Makers" list.

### Tag(s)

- **Families** : The kind of karaoke
	- For information and to avoid confusion, the tag "Anime" encompasses EVERYTHING animated no matter it's a japanese anime, an american cartoon, a french production, etc. If your video isn't made with any animation methods you have to tick "real-life"
- **Plateform(s)** : For video games: on which platform(s) was the game released?
- **Genre(s)** : What kind of creation is it?
- **Origin(s)** : What's the karaoke from?
- **Other(s)** : Other tags. The "Long" tag is for karaokes lasting 5 minutes at least, usually.
- **Group(s)** : This allows you to group karaokes so they are downloaded together at once with the download manager. Even if you don't add anything, KM will automatically add a tag with the decade your karaoke's from.
- **Warnings** : This allows you to specify if the song can be problematic for viewers. For example because of Photo-Sensitivity Epilepsy (PSE), Spoilers, or if it's a song for adults only.
- **Collection(s)** : Which collection the karaoke belongs to? Depending on where it comes from, its language, its type, it'll go into one collection but not another. Users might not see the song if it's in a collection they haven't enabled.

## Karaoke file validation and creation

Once all the fields are filled in, you just have to click on the big blue button `Save` to generate your kara file.

If everything went well, you should see a small notification at the top of the app.

The kara file that you’ve just created was put into your primary `karaokes` folder, and video, lyrics from the *.ass* files have been renamed according to what you put in the form and placed respectively in the `medias` and `lyrics` folders (or the first of them if you have specified several ones in your `config.yml` file).

To find them among other files, sort them by modification date in your file explorer.

If you want to modify a karaoke file, [go back to this page](../create-karaoke/editkaraoke/#modifying-metadata-for-a-karaoke-or-series)

## Pro-tips

This section is specific to Karaoke Mugen's karaoke base.

### References

{{% include "includes/inc_series-names.md" %}}

### Typography

Some small details on how to fill some tags :

#### Song title

**All** symbols / special characters are supported. So do not hesitate to use the exact original song titles, with double dots, exclamation marks, hearts, stars, etc. ( ex : "Van!shment Th!s World" ; "Sparkle☆彡Star☆Twinkle Pretty Cure" ; "shØut")

#### Singer(s) and Songwriter(s)

**[Use anidb!](https://anidb.net/perl-bin/animedb.pl?show=song&songid=63520)** and indicate the name of the artist *as it's officially retranscribed*. It would be tempting to write **_Être_** with a uppercase letter, but the official spelling is  `Black Raison d'être`, followed by `Chinatsu Akasaki, Azumi Asakura, Mâya Uchida, Sumire Uesaka` (click on the artist’s name to check on the *"official name"* line).

There sometimes are music bands with several personalities (ex: the [FictionJunction](https://en.wikipedia.org/wiki/FictionJunction) band by Yuki Kajiura) with singing characters (ex: most idol anime). In that case, You must as much information as you can find : **Add the band's name and singers** but let's keep things reasonable and do not add all 48 singers in AKB48.

If the singers are actually the series' characters, add the **voice actors' names.**

#### Creator(s)

If you can't find the creator you wish to add, check out [the "Studios / Creators" section from the references pages](./references.md).

### Rules

When a song is linked to a series, you should add the series to the song (in the **series** field) even if it's a remix or cover that isn't in the series in question.

When you add:

- A song title
- A person (as singer or songwriter)
- A series

Take into account the following typography rules:

{{% include "includes/inc_japanese.md" %}}

Those rules are **mandatory** for the fields `Song title` (**_Shinzô wo Sasageyo_**), `Serie(s)` (**_Chûnibyô_**), `Singers(s)` (**_Mâya Uchida_**), `Songwriter(s)` (**_Yôko Kanno_**).

#### Specific rules

If you add an entry that does not exist yet in the database, **always** write "First Name" then "Last Name". to avoid mistakes, check anidb (https://anidb.net/) as it always gives the name as "Last Name" followed by "First Name", so all you have to do is reverse the order. MyAnimeList does the same with its names. If in doubt, run the name in Kanji through Google Translate and look at the transcription, then reverse the order.

![ass](/images/creation/nameGTrad.jpg)