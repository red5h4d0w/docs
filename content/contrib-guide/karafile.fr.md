+++
title = "Les fichiers karaoké"
weight = 4
+++

Les informations d'un karaoké sont stockées dans un fichier de métadonnées. Il est primordial : il indique à Karaoke Mugen où sont les fichiers à utiliser ainsi que d'autres infos pour générer sa base de données.

On y trouve aussi toutes les métadonnées concernant votre karaoké : le nom du chanteur, du parolier, du studio ayant réalisé la vidéo, la date de création ou la langue utilisée pour chanter.

Ici, nous allons voir comment créer ou modifier ce fichier via une interface graphique.

## Accès à l'interface

- L'interface de gestion des karaokés se trouve dans Karaoke Mugen. Lancez le programme et sur l'écran d'accueil, cliquez sur le bouton **Panneau Système** (vous pouvez aussi y accéder via l'onglet en haut de la fenêtre **Allez à** > **Panneau Système**).

![ass](/images/creation/System-FR.jpg)

- Une fois dans la partie système, cliquez sur le 3e onglet **Karaokés** puis cliquez sur **Nouveau** pour accéder au formulaire de création de karaoké.

![ass](/images/creation/SystemTab-FR.png)

## Remplir les informations du karaoké

Vous voilà donc dans le formulaire de création de kara.

![ass](/images/creation/Karacreatepage-FR.png)

Les informations contenus dans le fichier karaoké sont écrits sous la forme de tags. Les tags ont leur propre [fichier de données](../tag/), qui contient les éventuelles variations régionales et alias.

Soit vous avez une liste de cases à cocher (auquel cas vous pouvez cocher les cases appropriées), soit vous avez un champ avec auto-complétion qui vous proposera soit de cliquer sur un tag existant, soit de créer un tag directement.

Si vous êtes amené à créer des tags, vous pourrez, après la création de votre karaoké, éditer lesdits tags pour ajouter des informations supplémentaires.

**Passons en revue le formulaire pour mieux le comprendre :** 

### Fichier vidéo

C'est votre source vidéo (ou audio si vous avez timé un mp3). Cliquez sur le bouton et allez chercher votre fichier pour l'ajouter (vous pouvez aussi le drag & drop directement).

### Fichier de sous-titres

C'est votre *time*, votre fichier *ASS*. Cliquez sur le bouton et allez chercher votre fichier pour l'ajouter (vous pouvez aussi le drag & drop directement).

### Karaokés parents

Ce concept de kara parents / enfants sert à regrouper les karaokés portants sur la même chanson, sans pour autant que ces derniers ne proviennent de la même source vidéo. Voilà un exemple pour mieux comprendre :  

Le kara **JPN - Shin Seiki Evangelion - OP - Zankoku na Tenshi no These** est un kara parent, et tous les autres karas de cette chanson sont ses enfants, que ça soit les reprises, les AMV, les concerts, etc.  
  
Dans cette section, en selectionnant un karaoké parent déjà existant, non seulement cela copie les informations du parent sur votre karaoké que vous être en train de créer, mais le définit également comme "enfant" du karaoké parent. Cela veut dire que les utilisateurs verront que le parent a différentes versions enfant. Il peut s'agir de toutes les versions différentes de l'opening de Sakura Wars, ou bien encore une *music video* qui a comme parent un *opening*.

En sélectionnant le parent, les visiteurs peuvent plus facilement découvrir qu'il existe des versions alternatives.

Un karaoké peut avoir plusieurs parents.

### Titre du morceau

C'est le titre de la chanson de votre karaoké. Il peut exister en plusieurs langues et avoir des alias. Les alias servent au moteur de recherche et vous pouvez indiquer des mots qui ne se trouvent pas déjà dans le(s) titre(s) du karaoké.

Vous pouvez ajouter plusieurs titres pour un même karaoké. Attention à ne pas donner de traduction non-officielle telle qu'on peut trouver sur les wikia de fans entre autres.

Avant d'ajouter un titre, vous devez d'abord indiquer dans quelle langue vous allez l'ajouter.

Dans le cas d'un karaoké d'animé par exemple, ajoutez la langue "Japonais (JPN)" et renseignez le titre **en caractère japonais (katakana ou hiragana)**. Vous pouvez également ajouter sa retranscription en romaji dans la langue "Alphabet Latin (Romanisation) (QRO)".  
Dans le cas d'un ajout à la base commune de KM, l'ajout de la retranscription est **obligatoire**.

Enfin, sélectionnez la langue par défaut du karaoké, à savoir la langue de son pays d'origine. Dans le cas d'un ajout à la base commune de KM, **ne mettez JAMAIS la langue "Japonais (JPN)" par défaut.**

### Alias

En complément des titres, vous pouvez ajouter des alias. Ils ne s'afficheront pas dans l'application mais seront quand même pris en compte lors d'une recherche. **Ajoutez des alias qui sont utiles** et qui contiennent des mots qui ne sont pas dans les titres renseignés plus haut. 

Exemple : vous avez rajouté le kara **Ai Kotoba Ⅳ** avec **Love Words IV** en anglais. Vous pouvez mettre un simple `4` en alias. Cela suffira à KM pour trouver le kara si vous cherchez "Love Words 4"

Vous pouvez mettre un simple `4` en alias car les mots "Ai", "Kotoba", "Love" et "Words" sont déjà pris en compte par le moteur de recherche (puisqu'ils sont dans les titres), inutile de les rajouter une seconde fois.

### Langues(s)

La langue dans laquelle votre karaoké est interprêté. Une liste existe déjà, il suffit de commencer à taper la langue et de sélectionner l'un des résultats qui ressort.

- Si c'est une langue inconnue / inventée, ajoutez la langue "Langue inconnue".
- Si votre karaoké ne contient aucun chant de base (et donc que votre fichier ASS est vide), ajoutez la langue "Pas de contenu linguistique".
- **Vous pouvez ajouter jusqu'à 2 langues sur le même karaoké. Passé ce chiffre, vous devez mettre uniquement la langue "Multi-langues".**
- Si votre karaoké compte autant ou *quasi* autant de lignes <langue 1> que de lignes <langue 2>, indiquez les 2 langues. À l'inverse, si une langue domine largement sur l'autre, n'indiquez que celle-ci.
- Si vous avez timé une version *instrumentale* (donc sans parole), indiquez tout de même la langue dans laquelle le kara sera chanté par le public.

### Œuvre(s)

C'est ici que vous ajoutez l'œuvre dont est issue votre karaoké (ou plusieurs si vous faites des AMV par exemple).

Cliquez sur **Ajout** et commencez à taper son titre, vous verrez alors des suggestion de séries déjà présentes dans la base.

Référez-vous [aux normes de typographie plus bas.](karafile.md#typographie)

![ass](/images/creation/KaracreatepageSerie-FR.png)

Si vous trouvez votre série dans la liste, cliquez dessus et elle s'ajoutera au formulaire. S'il s'avère que votre série n'existe pas encore, renseignez-la (son nom d'origine) et ajoutez-là avec le bouton bleu `Créer un tag`, un tag va être créé automatiquement avec ce nom. Vous pourrez ensuite apporter des informations supplémentaires à ce tag dans l'onglet dédié aux tags de la base.

**Ecrivez le nom des séries dans la langue du pays dans lequel elles ont été crées.** (par exemple "Hagane no Renkinjutsushi" et pas "Fullmetal Alchemist")

### Franchise(s)

Ce champ est à remplir si vous ajoutez un karaoké lié à une franchise globale.  
Exemple :  Si vous ajoutez l'ending de l'œuvre **Sound! Euphonium saison 2**, alors il faut lier le kara à la franchise **Sound! Euphonium**

### Types de chanson

C'est ici que vous indiquez dans quel contexte la vidéo a été crée. Vous avez le choix entre : Générique de début, générique de fin, insert song, Music Video, AMV, vidéo promotionnelle (PV), pub (CM), concert, character song, image song, audio uniquement, replay de stream ou diver / inclassable

{{% expand "A propos des vidéos amateur" "false" %}}
**Une vidéo non officielle / amateur est une AMV. Une vidéo officielle est une MV.**
{{% /expand %}}

{{% expand "A propos des karaokés audio uniquement" "false" %}}
Si vous ajoutez un MP3, cochez la case **Audio uniquement**, puis choisissez en complément le type qui convient le mieux à la chanson. Si par exemple c'est une chanson que l'on entend dans la série (même dans une version raccourcie), mettez *Opening*, *Ending* ou *Insert Song* en fonction. Si c'est une version complète, cochez également la case *Complète* dans la section [Versions](#versions). 
{{% /expand %}}

{{% expand "A propos des character song et image song" "false" %}}

- Une Image Song est une chanson utilisée à but promotionnel et **non utilisée** dans l'anime (par exemple l'album "Negai ga Kanau Basho ~Vocal&Harmony version~" de CLANNAD est un image album et contient donc des image songs).
- Une Character Song est une chanson utilisée à but promotionnel, non utilisé dans l'anime et qui a pour vocation d'approfondir le personnage qu'elle va traiter. Elle n'est **pas forcément chantée** par le seiyû du personnage traité. On trouve des Character Song soit dans des albums qui leur sont exclusivement dédiés, soit ajoutées dans les singles ou l'OST de l'anime.
- À ne pas confondre avec une Character Version, qui est une reprise d'une chanson, chantée par un personnage différent de la performance originale (exemple, le Hare Hare Yukai chanté par Kyon). Dans ce cas-là ne pas oublier d'ajouter `~ [nom du personnage] Vers.`.

{{% /expand %}}

N'utilisez **autre** qu'en dernier recours.

### Numéro de chanson

Les numéros de chanson n'affectent en général que les chansons issues d'animés japonais.

{{% expand "A propos des numéros de chanson" "false" %}}

- Cette section sert à numéroter **uniquement les openings et les endings**, nous ne numérotons pas le reste.

- Si la série ne contient qu'un seul opening / ending, ce champ doit être **vide** (et pas "0").

- Sur anidb, si le premier épisode utilise comme ending la musique qui deviendra l'opening dans les épisodes suivants, n'en tenez PAS compte, ce n'est PAS un ending, cela reste avant tout l'opening de la série.

{{% /expand %}}

### Versions

Les tags *Versions* vous permettent de préciser une altération d'une chanson, pratique pour les cas où plusieurs karaokés représentent la même chanson, mais que c'est une reprise, une version instrumentale, etc.

Lorsque vous allez créer votre karaoké, les versions vont être disponibles sous forme de cases à cocher (vous pouvez en cocher plusieurs). Si vous devez préciser une variation ne figurant pas dans la liste (comme par exemple une variation liée à un épisode dans la série ou la même chanson mais chantée par un autre personnage), vous pouvez le préciser après le titre de la chanson en adoptant la forme `<titre> ~ <version> Vers.`.  
  
Par exemple [irony ~ Ep. 12 Vers.](https://kara.moe/base/kara/irony-ep-12-vers/b9e80280-0d04-415a-bca3-2fd2681043e2)  
Remarquez que le tag alternative est présent.

Ce n'est pas parce que vous précisez manuellement une version dans le titre, que vous ne pouvez pas mettre de tag version (ceux des cases à cocher).


{{% expand "A propos du tag *Complète*, *Courte*, des concerts, de la vie et de tout le reste" "false" %}}
- Si l'on ajoute une chanson en version *complète*, on coche la case *Complète* uniquement si cette chanson existe déjà dans la base sous une autre forme (dans la majeure partie des cas, en version courte).  

Exemple :  

- `ENG - Kiznaiver - OP - LAY YOUR HANDS ON ME` et  
- `ENG - Kiznaiver - MV - LAY YOUR HANDS ON ME [Version Complète]`

- On ne met jamais le tag *Courte* sur un opening ou ending d'1m30.

{{% /expand %}}

### Chanteur(s)

Le(s) interprète(s) de votre karaoké.  

**Référez-vous [aux normes de typographie plus bas.](#typographie)**

### Groupe musical

Cette section facultative sert à indiquer le nom du groupe qui interprète la chanson. Il peut s'agir d'un vrai groupe (BTS, ASIAN KUNG-FU GENERATION, JAM Project) ou d'un groupe "fictif" crée pour une œuvre spécifique (Kessoku Band, µ's, Hô-kago Tea Time, Nightcord at 25:00, hololive IDOL PROJECT)

### Compositeurs(s)

Le(s) auteur(s), compositeur(s), arrangeur(s) **et** parolier(s) de votre karaoké. Comme les œuvres, une liste existe déjà, vérifiez que vos compositeurs ne sont pas déjà dedans pour ne pas créer un doublon.  

**Référez-vous [aux normes de typographie plus bas.](#typographie)**

### Créateur(s)

L'entité à l'origine de votre vidéo (souvent des studios d'animation ou de développement de jeux). Comme les séries, une liste existe déjà.

{{% notice note "Selon votre type de chanson..." %}}
- Si vous ajoutez une AMV, mettez le studio **et** le créateur de l'AMV.
- Pour les jeux vidéo, mettez l'entité qui a **développé** le jeu, pas celle qui le vend / publie.
- Si c'est une vidéo live avec des vrais gens, mettez le label qui commercialise la chanson en question.
{{% /notice %}}  

**Référez-vous [aux normes de typographie plus bas.](#typographie)**

### Année de diffusion

L'année **où votre vidéo a été diffusé / crée** (et non l'année de commencement de votre anime, pas de "1999" sur l'OP 18 de One Piece).

### Autres tags

- **Collection(s)** : A quelle collection le karaoké appartient ? Selon sa provenance, sa langue, son type, il ira dans une collection mais pas dans une autre. Les utilisateurs pourraient ne pas voir le karaoké s'il est dans une collection qu'ils ont cachée.
- **Famille(s)** : Type de famille du karaoké.
	- À titre d'information et pour éviter les confusions, le tag "Anime" concerne TOUT ce qui est animé, que ça soit un anime japonais, un cartoon américain, une production française, etc. Si votre vidéo n'est pas fait avec une technique d'animation, vous devez donc cocher "Prise de vue réelle".
- **Plateforme(s)** : Pour les kara de jeux vidéo, sur quelle(s) plateforme(s) le jeu est-il sorti ?
- **Genres** : Quel genre d'œuvre est-ce ?
- **Origine(s)** : D'où vient le karaoké ?
- **Divers** : Le tag "Long" est réservé aux karaoké qui durent 5 minutes ou plus (ne vous en souciez pas, il sera automatiquement ajouté si KM détecte plus de 300 secondes sur votre vidéo).
- **Groupe(s)** : Permet de regrouper plusieurs kara dans des "familles". Même en ne rentrant rien, KM ajoutera automatiquement un tag indiquant la décennie d'où vient votre karaoké.
- **Warning(s)** : Permet d'indiquer que ce karaoké peut potentiellement poser des problèmes à un public (par exemple des flashs rapides, spoiler une œuvre ou du contenu pour adulte).

### Auteur(s) du karaoké

C'est vous ! Tapez votre nom ou pseudo pour vous ajouter dans la liste des "Karaoke Maker".

### Dépôt

Dans quel dépôt souhaitez-vous ajouter ce kara ? Vous pouvez choisir de le mettre dans votre base local customisée, ou bien dans celle de Karaoke Mugen pour le partager au monde entier (kara.moe).

## Pro-tips de remplissage des champs

Cette section concerne principalement les règles et autres exceptions de la base karaokés officielle de Karaoke Mugen.

### Références

{{% include "includes/inc_series-names.fr.md" %}}

### Typographie

Des petits détails sur le contenu de certains champs :

#### Titres du morceau

**Tous** les symboles / caractères spéciaux sont tolérés. N'hésitez donc pas à utiliser les noms originaux des chansons, avec des 2 points, des points d'exclamation, des cœurs, des étoiles, etc. ( ex : "Van!shment Th!s World" ; "Sparkle☆彡Star☆Twinkle Pretty Cure" ; "shØut")

Si votre chanson n'a pas de titre officiel **mais** est une reprise de la chanson originale dans une langue différente, mettez le nom de la chanson originale. Par exemple, tous les génériques français de Dorémi ont reprit les originaux japonais, ce qui donne `FRE - Ojamajo Doremi - OP - Ojamajo Carnival!!`

#### Chanteur(s) et compositeur(s)

**[Utilisez de préférence anidb](https://anidb.net/perl-bin/animedb.pl?show=song&songid=63520)** et indiquez le nom de l'artiste *tel qu'il est retranscrit officiellement*. Si aucun artiste ne vous est suggéré par l'application, c'est qu'il n'est pas dans la base de données à l'instant T (ou que vous l'avez mal écrit). S'il n'existe pas, continuez de taper son nom et validez avec "Ajout". L'artiste sera ainsi crée lorsque vous aurez terminé et validé le formulaire. Comme dis plus haut, veillez à bien écrire le nom. Il serait tentant d'écrire par exemple **_Être_** avec un e majuscule, comme le reste du nom de groupe, mais l'écriture officielle est bien [Black Raison d'être](https://anidb.net/creator/38790) (cf la ligne *"official name"*).

Dans le cas où ce sont les personnages d'une série qui chantent, on indiquera **le nom des comédiens de doublage.**

#### Créateur(s)

La liste de suggestions est déjà bien complète, mais si le nom que vous souhaitez ajouter n'est pas dans la liste déroulante, aidez-vous de [la section "Studios / Créateurs" de la page Références](../references) listant la majorité des studios d'animation / de jeux vidéo / de toku / de films live, avec leur nom **officiel**.

### Règles

Dès lors qu'une chanson est liée à une oeuvre, on renseigne l'oeuvre (dans le champ **série** donc), même si c'est une reprise qui n'est pas audible dans l'oeuvre en question.

Lorsque vous ajoutez :

- Un titre de chanson
- Une nouvelle personne (en tant que chanteur ou compositeur)
- Une nouvelle œuvre

... prenez en compte ces quelques notes sur la typographie des noms en japonais :

{{% include "includes/inc_japanese.fr.md" %}}

### Règles particulières

En restant dans le cas où vous ajoutez une entrée qui n'existe pas encore dans la base de données, **toujours** écrire "Prénom" puis "Nom". Pour ne pas se tromper, anidb indique toujours les artistes en "Nom" "Prénom", il suffit donc d'inverser. MyAnimeList fait la même chose sur ses fiches. Si vous avez un doute, passez le nom en kanji dans Google Trad et regardez la retranscription, puis inversez l'ordre.

![ass](/images/creation/nameGTrad.jpg)

### Validation et création du fichier kara

Une fois que tous les champs sont remplis, il ne vous reste plus qu'à cliquer sur le gros bouton bleu  `Sauvegarder` pour générer le fichier de votre kara.

Si tout s'est bien passé, vous devriez voir apparaitre une petite notification en haut de l'appli.

Le fichier kara que vous venez de créer a été placé dans votre dossier primaire "karaokes", et la vidéo et le fichier *.ass* ont été renommés selon ce que vous avez indiqué dans le formulaire, et placés respectivement dans les dossiers "medias" et "lyrics" (ou dans le premier d'entre eux si vous en avez indiqué plusieurs dans votre fichier `config.yml`).

Pour les retrouver parmi tous les autres fichiers, triez-les par date de modification dans votre explorateur de fichiers.

Aussi, votre nouveau karaoké a été ajouté à votre base de données sans que vous ayez à régénérer celle-ci, ce qui vous permet de tester tout de suite votre ajout.

Si vous souhaitez modifier un fichier kara, [rendez-vous sur cette page.](../create-karaoke/editkaraoke#modifier-les-informations-dun-karaoké-ou-dune-série)
