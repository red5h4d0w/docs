+++
title = "Participer à la base"
weight = 1
+++

Si vous n'êtes pas développeur, que fabriquer un karaoké cétrokompliké, mais que vous souhaitez tout de même nous aider sur le projet, il y a plein de choses à régler dans la base actuelle.

## Améliorer les données

Ce qui fait une bonne base de données, c'est avant tout la richesse de son contenu. Une base de données que l'on peut trier et ausculter sous de nombreux critères est idéale. On a justement [une section entière du site web](https://kara.moe) dédiée à cela.

Grâce à cet explorateur, vous pouvez déjà savoir, par exemple, combien de [chansons en coréen](https://kara.moe/search/?q=t%3A3b9e2749-27d0-4a78-873d-ea91a9ee9a25~5) sont présentes dans la base, ou combien de musiques ont été [composées par Yoko Kanno](https://kara.moe/search/?q=t%3Aaf0be06d-ea2d-4f46-85d1-58cf9f42532a~8), ou encore combien de karaokés sont [tirés de jeux de la PlayStation première du nom](https://kara.moe/search/?q=t%3Af84b68ac-943a-495a-9c6e-d90ec959b19e~13). Ces informations sont comprises dans les fichiers **.kara.json** et **.tag.json**. Nous avons rempli certains d'entre eux avec les données que nous connaissions, mais nous avons de nouveaux fichiers qui arrivent régulièrement (et qui mériteraient d'être mis à jour).

* Pour compléter les données manquantes, plusieurs possibilités : soit vous signalez ce qu'il manque avec le lien en bas de la fiche d'un karaoké précis et vous priez pour que quelqu'un fasse les modifications pour vous, soit vous proposez directement les modifications via le formulaire d'édition de karaoké en bas de la page.
* Il y a [quelques karaokés notoirement mal timés](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?label_name[]=lyrics). C’est-à-dire que les paroles n’apparaissent pas forcément au bon moment pour chanter, ou il en manque, etc. S'il fait déjà partie de la liste vous pouvez [corriger le fichier `.ass`](../create-karaoke/karaoke) avant [de l'uploader](../create-karaoke/upload). Si ce n'est pas le cas, vous pouvez retrouver [sa fiche](https://kara.moe/base/kara/wake-up-get-up-get-out-there/08066ef4-6ef8-4e53-87a8-6a36c6459605) et utiliser le lien en bas pour nous le signaler !
* On a aussi [des vidéos de très mauvaise qualité](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?label_name[]=media), en petite résolution ou autre. Pour ça, il faut parfois chercher loin pour en trouver de meilleures. Et parfois, les gens comme vous en ont déjà des stocks chez eux. N'hésitez pas à nous le signaler si c'est le cas.
* Enfin, nous avons [une liste de suggestions](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=suggestion) ! Une chanson vous plaît, pas de problème, faites une suggestion en créant une nouvelle issue et priez pour que quelqu’un s’en occupe. Les suggestions **prioritaires** sont pour des karas suceptibles d'être demandés en convention (des chansons populaires de saison par exemple).
* Notez qu’on est jamais aussi bien servi que par soi-même, c’est pour ça qu’il y a [un tutoriel](../create-karaoke) !
* Et si vous pensez avoir trouvé une piste qui bug, allez [voir la liste globale des issues](https://gitlab.com/karaokemugen/karaokebase/issues), quelqu'un l'a peut-être déjà signalé avant vous. Sinon, vous pouvez utiliser [sa fiche](http://kara.moe/kara?kid=08066ef4-6ef8-4e53-87a8-6a36c6459605) et utiliser le lien en bas pour nous l'indiquer.

## Devenir mainteneur

Si vous pensez être prêt à nous aider sur la base de temps à autres, n'hésitez pas à nous [envoyer un petit message](https://mugen.karaokes.moe/contact.html), à vous inscrire sur [le forum](https://discourse.karaokes.moe) et/ou à rejoindre [le Discord](https://karaokes.moe/discord), on sera ravis de vous aider à nous aider ! On vous apprendra à utiliser Git et d'autres outils.
