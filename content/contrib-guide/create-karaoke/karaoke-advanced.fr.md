+++
title = "Aegisub avancé"
weight = 5
+++

Vous avez pu découvrir comment créer un karaoké de A à Z via [le tutoriel](../karaoke). Ici, on va s'attarder sur les outils Aegisub pas forcément obligatoires à utiliser, mais très pratiques par exemple pour corriger des sous-titres existants.

## Le gestionnaire de styles

Vous pouvez accéder à ce gestionnaire via **Sous-titres > Gestionnaire de styles**. La fenêtre suivante s'affichera alors :

![Styles](/images/creation/GestionnaireStyles.png)

Concentrons-nous sur la section **Script actif**. C'est ici que vous gérez les styles que vous souhaitez utiliser pour votre fichier de paroles. Vous pouvez les éditer, les copier dans votre banque personnelle **Stockage**, les trier par ordre alphabétique / changer l'ordre, les supprimer ou les copier pour ensuite en créer de nouveaux.

La section de gauche **Stockage** est grosso modo la mémoire de votre Aegisub, c'est une banque de style propre à votre installation. Vous pouvez y enregistrer des styles pour une utilisation futur, et cliquer sur le bouton *"Copier dans le script actif"* pour les rendre actifs.

{{% notice info "Les styles -furigana" %}}
Les styles "-furigana" sont générés par les scripts d'automatismes à partir des styles de base, il faut penser à les supprimer après avoir fini car il ne sont pas du tout utiles.
{{% /notice %}}

### Modifier un style

Il se peut que vous ayez envie de modifier un style : tenter une autre couleur, taille trop grande ou trop petite à votre goût...
Pour modifier un style, sélectionnez-le dans le gestionnaire de styles et cliquez sur **Modifier** (ou double-cliquez dessus). Vous aurez alors l'écran suivant :

![Style](/images/creation/EditStyle.png)

Détaillons chaque champ :

- **Nom du style** : Gardez le nom par défaut si vous n'avez qu'un style, ou explicitez selon ce que vous voulez faire. Genre "KM-haut", "KM-chœur", etc. Ça vous évitera de vous perdre parmi plusieurs styles.
- **Police** : Arial est très bien puisqu'installée sur tous les systèmes, en plus d'être lisible. Un peu plus à droite, vous pouvez changer sa taille, par contre. Pratique si vos sous-titres sont trop petits... ou s'ils sont un poil trop gros et que ça fait s'afficher un sous-titre sur deux lignes.
- **Couleur primaire** : la couleur primaire est la couleur de base de l'intérieur de votre sous-titre. Ou dans le cas d'un karaoké, c'est la couleur qui sera affichée après que la syllabe ait été chantée.
- **Couleur secondaire** : utilisée uniquement dans le cas d'un karaoké, c'est la couleur qui sera affichée avant que la syllabe ne soit chantée.
- **Couleur de contour** : c'est la couleur qui entourera la base de votre sous-titre. C'est très bien le noir.
- **Couleur d'ombre** :  c'est la couleur qui donnera une sorte d'ombre à votre sous-titre. On n'en met pas dans nos samples, mais ça n'est pas interdit. *À régler avec le paramètre "Ombre" à droite*
- **Marges** : c'est ce qui évite que votre sous-titre ne soit trop collé à gauche, à droite, ou au haut/bas de votre vidéo. C'est possible de jouer un peu avec, mais évitez la valeur 0. Vous pouvez faire joujou avec pour afficher vos lignes à peu près n'importe où sur la vidéo
- **Alignement** : vous pouvez changer l'emplacement par défaut de votre sous-titre, chaque valeur entre 1 et 9 étant affectée à une position conforme à ce qui est montré ici. Le point d'ancrage votre sous-titre changera en conséquence.
- **Contour** : vous pouvez grossir ou rétrécir le contour et/ou l'ombre de vos sous-titres ici.
- **Divers** : vous pouvez définir une rotation globale ou agrandir l'écart entre deux caractères. **Ne touchez pas aux échelles X et Y.**

{{% notice warning "Attention" %}}
Lorsque vous modifiez un style, de bien garder en tête que la lisibilité est primordiale. 

Attention donc aux couleurs trop fantaisiques, par exemple.
{{% /notice %}}

### Changer une couleur

L'écran de changement de couleur est le même, que vous cherchiez à changer la principale, la secondaire, celle de contour ou encore celle d'ombre.

![Couleur](/images/creation/Couleur.png)

Vous pouvez changer la couleur, avec la réglette puis avec le spectre, ou directement renseigner la définition RGB de la couleur que vous recherchez. À noter qu'il aussi possible de changer le canal **"Alpha"** de votre couleur, c'est-à-dire sa transparence. Toutes les couleurs sont définies sur un octet, c'est à dire de 0 à 255 ou de 00 à FF selon que votre définition utilise le système décimal ou hexadécimal. Vous avez aussi une "pipette" à disposition pour récupérer une couleur n'importe où sur votre écran.

Les définitions ASS que l'on utilisera fréquemment sont :

- Orange : `&H0088EF&`
- Bleu : `&HEF9900&`
- Rouge : `&H5555FF&`
- Vert : `&H66EF22&`

{{% notice warning "Attention à la lisibilité" %}}
Gardez à l'esprit que la différence entre la couleur primaire et secondaire doit être suffisament élevée pour avoir un bon contraste afin que les utilisateurs puissent clairement identifier quand il faut chanter, et ce même de loin ou avec un reflet sur l'écran.

Selon la vidéo que vous utilisez, il sera peut-être plus pertinent de mettre par exemple une couleur claire pour bien faire ressortir les paroles si la vidéo a un fond plutôt sombre.
{{% /notice %}}

## Le décalage temporel

Une fonction très utile lorsque les sous-titres semblent uniformément décalés par rapport à une vidéo, c'est le décalage temporel. Passage quasi obligatoire en cas de remplacement de vidéo par une autre de meilleure qualité.

Rendez-vous dans **Timing > Décalage temporel**. L'écran suivant apparaît alors :

![Temp](/images/creation/DecalageTemporel.png)

Il est possible alors de définir la durée de décalage que l'on souhaite appliquer aux sous-titres au 1/100e de seconde près, en avant comme en arrière.

Faites un premier essai de décalage en regardant ce que ça donne via le spectre audio *en mode karaoké*, si ça ne vous convient pas, faites un CTRL+Z et retentez avec une autre durée.

Il est possible d'appliquer le décalage à tous les sous-titres ou seulement à une plage présélectionnée auparavant, mais aussi de l'appliquer seulement aux minutages de début ou de fin, pour rajouter du temps d'affichage.

### Convertir la vitesse des sous-titres

Parfois, il arrive que vos sous-titres se désynchronisent petit à petit au fil de la vidéo. Cela est lié aux FPS (frames/images par seconde) de la vidéo, la synchronisation ayant été originellement produite sur une vidéo avec un nombre de FPS différent, ce que Aegisub interprète comme une vitesse différente. Il faut donc convertir vos sous-titres.

Les formats les plus courants de FPS en vidéo sont 25 FPS et 23.97 FPS, correspondant au [PAL](https://fr.wikipedia.org/wiki/Phase_Alternating_Line) et au [NTSC](https://fr.wikipedia.org/wiki/National_Television_System_Committee).

Suivez donc le cheminement suivant :

- **Fichier -> Exporter sous...**
- Cochez la case **Transformation défilement**
- Indiquez dans **Défilement d'entrée :** le nombre de FPS de la vidéo originale (souvent 25 FPS)
- Indiquez dans **Sortie :** le nombre de FPS de la nouvelle vidéo (souvent 23.97 FPS)
- Appuyez ensuite sur **Export...** et testez le nouveau karaoké, en n'oubliant pas le décalage temporel !

Si cela ne fonctionne pas dans un sens, essayez toujours dans l'autre. Si les valeurs 25 et 23.97 ne fonctionnent pas, et que vous n'avez aucune idée de la vitesse de la vidéo d'origine, pas de bol ! Va falloir refaire le karaoké.

![FPS](/images/creation/TransformFPS.png)

### Les scripts de karaoké

Aegisub possède un système de scripts notamment pour aider à la modification en masse. Ce sont des fichiers texte avec une extension en `.lua` et il en existe énormément sur internet. L'Aegisub customisé [de Japan7](../material) en contient quelques-un en particulier qui nous intéressent et peuvent tout à fait nous servir pour gagner un temps précieux. Passons-les en revue.

![Temp](/images/creation/scripts-lua.jpg)

{{% notice note "Les versions d'Aegisub" %}}
Si vous avez déjà installé la version "officielle" d'Aegisub, les script peuvent être directement récupérés en faisant un clic droit puis "Enregistrer sous" sur les noms suivants. Ils sont à mettre dans votre dossier Aegisub / automation / autoload.*
{{% /notice %}}

{{% notice tip "Trucs et astuces" %}}
Vous pouvez sélectionner plusieurs lignes de time pour appliquer le script voulu à toutes ces lignes en une seule fois.*
{{% /notice %}}

- [Clean k tags](/aegisub/clean-k-tags.lua) : Permet de fusionner des doubles balises inutiles en une seule. Par exemple `{\k6}{\k10}` devient `{\k16}`

- [Nihongo wa muzukashi (wohe.lua)](/aegisub/wohe.lua): Pour remplacer toutes les particules japonaises "o / ha / e" en "wo / wa / he".

- [Split karaoke line (karaoke-split.lua)](/aegisub/karaoke-split.lua) : Permet de découper une ou plusieurs lignes en deux tout en conservant les timings des syllabes. Pour ça, ajoutez `{split}` entre 2 mots d'une même ligne avant d'appliquer le script.

![Temp](/images/creation/split.jpg)

- [Mugenizer (karaoke-adjust-1sec.lua)](/aegisub/karaoke-adjust-1sec.lua) : Sans doute le plus pratique pour les vieux karas à retaper. Applique la résolution 0x0, ajoute la ligne pour avoir une seconde de décalage en fondu, et fixe les "découpes pré-chant" en réajustant le time en conséquence. [(voir la section *La synchronisation "par syllabe"*)](../karaoke)

- [auto-split](/aegisub/auto-split.lua) *(source différente, donc non inclus dans la version Japan7)* : Une fois que vous avez bien placé les temps de début et de fin de vos lignes, ce script permet en un clic de découper les syllabes à votre place, ce qui peut faire gagner du temps. **Attention**, cet automatisme ne marche *uniquement* que sur les mots japonais. Si vous l'appliquez sur des lignes qui contiennent un ou plusieurs mots anglais par exemple, il vous découpera ces derniers lettre par lettre. N'hésitez pas à repasser sur les découpes après coup car il vous découpe `zutto` en `zu | t | to` alors qu'il se peut vous n'aillez pas besoin de découper le mot en trois.
