+++
title = "Edit your karaoke"
weight = 6
+++

## Modifying to your subtitles' synchronization

You would like to modify your karaoke? Nothing simpler:

In Karaoke Mugen's System Panel, go to the karaoke list. You'll find a button there to edit lyrics on any song which will directly open Aegisub with the right associated files.

- Modify resolution in "**Properties**" and set it to 0 x 0.
- Select all **white** lines with the "fx" style and delete them.
- Select all **blue** lines left *except the last script line*, and untick "**Comments**" under the audio specter.

![ass](/images/creation/DeleteLines.gif)

- You are now back to the moment before you applied the script.
- Make your changes
- **Re-apply the karaoke template** in the "**Automatism**" tab
- Remove unused styles and furiganas in the tab "**Subtitles**" > "**Style Manager**"
- Save!
- Run the app, go to the system pannel, then edit your karaoke to put the new subtitles file in it.
   - You can also run the app with the option `--validate` or go to *Database** and then click on **Validate files**

## Changing the karaoke's video

- Run Karaoke Mugen
- Go to **System Panel**
- Go to the tab **Karaokes** then **List**
- Search for your karaoke and click on the edit button on the right.
- Swap the video for the new one with a "drag & drop" or press the button **Media File** which will open a dialog box.
- Save your changes by clicking **"Save"** at the bottom.

## Modifying metadata for a karaoke or series

### For a karaoke

- Run Karaoke Mugen
- Go to **System Panel**
- Go to the tab **Karaokes** then **List*
- Search for your karaoke and press the edit button on the right.
- Make your changes, remove entries using the small crosses on the tags to re-write them.
- Save by clicking the  **"Save"** button.

### For a tag

- Run Karaoke Mugen
- Go to **System Panel**
- Go to the tab **Tags** then **List**
- Search for your karaoke and press the edit button on the right.
- Make your changes, add languages and aliases with the  **Add** button, remove entries using the small crosses next to the tags to rewrite them.
- Save by clicking on the  **"Save"** button.

## Fixing double lines

That can happen if you do not split your lyrics into several parts enough, and those end up too long and cannot be displayed on only one line and have to be shown on two (or more).

![ass](/images/creation/KaraokeDoubleLineEdit.jpg)

2 solutions are available:

- You reduce the font size gradually until all of your double line can be displayed only on one. You can also add a real number too. **If, after the size 19 or 18, you still get double lines, come back to the regular size, and go for the second method down below.** One must not reduce too much the size or people will not be able to see anything while singing, especially at some distances.

- You can remove the shifting script and cut your double line into two disctinct parts. And so, here, you can make a line **"Cause I left behind the home"**, and a second one **"that you made me"** just after.

![ass](/images/creation/ChangeSizeFont.png)

## Fixing triple lines

This happens when one of your lines takes too much time to disappear before the (possibly) next upcoming line which forces it to appear on a third line at the top of the screen.

![ass](/images/creation/KaraokeTripleLineEdit.png)

You have two solutions:

- Changing the line cutting. For example here, you can totally rectify the error and display a first line **"kimi no sei, kimi no sei"** followed by a second one **"kimi no sei de watashi"**.

- You diminish the length of the long line staying on screen for too long. For example here, you reduce the duration of the first **"kimi no sei"** so that **"kimi no sei de watashi"** can appear as it should. You will notice however that this method isn't ideal: you *can* have a line that diseappears too early before it can be sung fully.

## Fade in/fade out script management

If your karaoke starts  **immediately**, your first timed line might appear too fast with not enough time for singers to prepare themselves to sing.

That's why it is better to remove the **fade in** (and to leave the fade out).

Take the first line with the "*fx*" effect and change the `fad(300,200)` values to `fad(0,200)` and that's all. Your line will appear and diseappar in 0.200 seconds.

![ass](/images/creation/DeleteFad1Line.gif)

## Removing unused styles

Once your karaoke's done (and maybe got fixed), it's important to remove all unused styles. For this, in Aegisub:

- Go to the **"Subtitles" > "Style Manager"** menu
- In the **"Active Script"** column, delete all styles ending in `furigana` and the ones that were not used (Down, Choir, Duo, etc).

![ass](/images/creation/DeleteScript.png)

## After any change

If you have changed a video, you have to send it to the Karaoke's Mugen FTP or directly to us [on Discord](https://karaokes.moe/discord) before sending the modified files.

To send your modified data:

- If you don't have access to the karaokes' git database, you can go to your song's page on [kara.moe](https://kara.moe) and offer your modification there.
- If you can access [the karaokes' git database](https://gitlab.com/karaokemugen/karaokebase) (you are a maintainer) make a **commit** with your files `.kara.json`, `.ass` and/or `.tag.json` if these were modified. Be **descriptive** about the modifications done. Once the **commit** done, you will have to **push** it to send it. To do this, go to System Panel, on the Maintainer tab and click on Git.
